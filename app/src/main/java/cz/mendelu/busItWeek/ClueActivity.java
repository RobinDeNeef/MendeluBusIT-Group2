package cz.mendelu.busItWeek;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cz.mendelu.busItWeek.R;
import cz.mendelu.busItWeek.library.StoryLine;
import cz.mendelu.busItWeek.library.Task;
import cz.mendelu.busItWeek.library.TaskStatus;

public class ClueActivity extends AppCompatActivity {

    private StoryLine _storyLine;
    private ListView _cluelist;
    //private Task _currentTask;
    //private ClueListAdapter _adapter;
    private List<Task> _tasklist;
    private int _score;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clue);
        _storyLine = StoryLine.open(this, MendeluMurderStoryLineDBHelper.class);
        _cluelist = (ListView) findViewById(R.id.clueList);
        _tasklist = _storyLine.taskList();
        _score = 0;


        // Populate Clues
        ArrayList<ClueItem> objects = new ArrayList<ClueItem>();
        for (Task task : _tasklist){
            String name = task.getName();
            if (!name.equals("Mendelu Murder Chapter VI: Revelations")) {
                String clue = "CLUE NOT FOUND";

                if (name.equals("Mendelu Murder Chapter I: Beginning") && (task.getTaskStatus().toString().equals("SUCCESS"))) {
                    clue = "Big footprints are found near the murderscene.";
                    Log.d("TAG CLUE", "CLUE");
                } else if (name.equals("Mendelu Murder Chapter II: Bury Your Dead") && (task.getTaskStatus().toString().equals("SUCCESS"))) {
                    clue = "A witness saw that the killer had darker hair. ";
                    Log.d("TAG CLUE", "CLUE");
                } else if (name.equals("Mendelu Murder Chapter III: Hush Hush") && (task.getTaskStatus().toString().equals("SUCCESS"))) {
                    clue = "A shaving razor was found in the proximity of the murderscene. ";
                    Log.d("TAG CLUE", "CLUE");
                } else if (name.equals("Mendelu Murder Chapter IV: Something Broken") && (task.getTaskStatus().toString().equals("SUCCESS"))) {
                    clue = "The killer lost some of it's jewelry.";
                    Log.d("TAG CLUE", "CLUE");
                } else if (name.equals("Mendelu Murder Chapter V: Live By Night") && (task.getTaskStatus().toString().equals("SUCCESS"))) {
                    clue = "The killer escaped through a hole in the fence.";
                    Log.d("TAG CLUE", "CLUE");
                }
                ClueItem newClue = new ClueItem(name, clue);
                Log.d("TAG CLUE", name + "|" + clue);
                objects.add(newClue);
            }
            if (name.equals("Mendelu Murder Chapter VI: Revelations")&& (task.getTaskStatus().toString().equals("SUCCESS"))){
                TextView killerText = (TextView) findViewById(R.id.killerStatus);
                killerText.setText("You have found the killer!");
            }

            if (task.getTaskStatus() == TaskStatus.SUCCESS){
                _score = _score + task.getVictoryPoints();
            }
        }

        TextView scoreText = (TextView) findViewById(R.id.score);
        scoreText.setText(("Score: " +_score));

        ClueAdapter customAdapter = new ClueAdapter(this, objects);
        _cluelist.setAdapter(customAdapter);}

    protected void onResume() {
        super.onResume();
    }
}
