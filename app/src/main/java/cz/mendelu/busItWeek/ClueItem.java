package cz.mendelu.busItWeek;


public class ClueItem {
    private String name;
    private String clue;

    public ClueItem(String name, String clue) {
        this.name = name;
        this.clue = clue;
    }

    public String getName() {
        return name;
    }

    public String getClue() {
        return clue;
    }
}
