package cz.mendelu.busItWeek;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import cz.mendelu.busItWeek.library.SimplePuzzle;
import cz.mendelu.busItWeek.library.StoryLine;
import cz.mendelu.busItWeek.library.Task;

public class SimplePuzzleActivity extends AppCompatActivity {

    // Variables
    private TextView _taskName;
    private StoryLine _storyLine;
    private TextView _questView;
    private EditText _answerView;
    private SimplePuzzle _puzzle;

    // Public
    public void checkAnswer(View view){
        // Get user Answer
        String userAnswer = _answerView.getText().toString();

        // Get correct answer from Puzzle
        String correctAnswer = _puzzle.getAnswer();

        if (userAnswer.equalsIgnoreCase(correctAnswer)){
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast, (ViewGroup) findViewById(R.id.toast_layout_root));

            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText("Clue found!");

            Toast toast = new Toast(getApplicationContext());
            toast.setGravity(Gravity.BOTTOM, 10, 50);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();

            _storyLine.currentTask().finish(true);
            finish();
        } else {
            _answerView.setError(getString(R.string.err_no_number));
            _storyLine.currentTask().finish(false);
            finish();
        }
    }

    // Protected
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_puzzle);

        _taskName = (TextView) findViewById(R.id.taskName);
        _questView = (TextView) findViewById(R.id.questionTextView);
        _answerView = (EditText) findViewById(R.id.answerEditText);
        _storyLine = _storyLine.open(this, MendeluMurderStoryLineDBHelper.class);
    }

    @Override
    protected void onResume(){
        super.onResume();
        Task task = _storyLine.currentTask();

        if (task == null ){
            _taskName.setText(R.string.no_task);
        } else {
            _taskName.setText(task.getName());
        }

        _puzzle = (SimplePuzzle) task.getPuzzle();

        _questView.setText(_puzzle.getQuestion());
    }
}
