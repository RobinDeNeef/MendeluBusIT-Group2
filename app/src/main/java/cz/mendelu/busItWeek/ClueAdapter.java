package cz.mendelu.busItWeek;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ClueAdapter extends BaseAdapter{
    private LayoutInflater inflater;
    private ArrayList<ClueItem> objects;

    private class ViewHolder {
        TextView nameLine;
        TextView clueLine;
    }

    public ClueAdapter(Context context, ArrayList<ClueItem> objects) {
        inflater = LayoutInflater.from(context);
        this.objects = objects;
    }

    public int getCount() {
        return objects.size();
    }

    public ClueItem getItem(int position) {
        return objects.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.clue_list_row, null);
            holder.nameLine = (TextView) convertView.findViewById(R.id.firstLine);
            holder.clueLine = (TextView) convertView.findViewById(R.id.secondLine);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Log.d("TAG ADAPTER",objects.get(position).getName());
        holder.nameLine.setText(objects.get(position).getName());
        holder.clueLine.setText(objects.get(position).getClue());
        return convertView;
    }
}
