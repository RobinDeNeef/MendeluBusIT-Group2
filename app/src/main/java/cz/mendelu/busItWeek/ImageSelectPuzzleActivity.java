package cz.mendelu.busItWeek;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.mendelu.busItWeek.library.ImageSelectPuzzle;
import cz.mendelu.busItWeek.library.StoryLine;
import cz.mendelu.busItWeek.library.Task;
import cz.mendelu.busItWeek.library.listview.ListViewUtils;
import cz.mendelu.busItWeek.library.timer.TimerUtil;

public class ImageSelectPuzzleActivity extends AppCompatActivity {
    // Variables
    private StoryLine _storyLine;
    private ImageSelectPuzzle _puzzle;
    private TextView _question;
    private TextView _counter;
    private TextView _taskName;
    private ListView _imageList;
    private Task _currentTask;
    private ImageSelectListAdapter _adapter;

    // Public
    public class ImageSelectListAdapter extends ArrayAdapter<Integer> implements AdapterView.OnItemClickListener{

        public ImageSelectListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Integer> objects) {
            super(context, resource, objects);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if(convertView == null){
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.image_select_list_row, null);
            }

            Integer image = getItem(position);

            if (image != null){
                ImageView imgView = (ImageView) convertView.findViewById(R.id.imageItem);
                imgView.setImageResource(image);
            }

            return convertView;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if(_puzzle.getAnswerForImage(position)){
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast, (ViewGroup) findViewById(R.id.toast_layout_root));

                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText("Clue found!");

                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 10, 50);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();

                _currentTask.finish(true);
                TimerUtil.stopTimer();
                finish();
            }else{
                _currentTask.finish(false);
                TimerUtil.stopTimer();
                finish();
            }
        }
    }

    // Protected
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_select_puzzle);
        _storyLine = StoryLine.open(this, MendeluMurderStoryLineDBHelper.class);
        _counter = (TextView) findViewById(R.id.counter);
        _question = (TextView) findViewById(R.id.question);
        _imageList = (ListView) findViewById(R.id.imagelist);
        _taskName = (TextView) findViewById(R.id.taskName);
        _currentTask = _storyLine.currentTask();
        _puzzle = (ImageSelectPuzzle) _currentTask.getPuzzle();

        List<Integer> listOfItems = new ArrayList<>();

        for (Map.Entry<Integer, Boolean> entry : _puzzle.getImages().entrySet()){
            listOfItems.add(entry.getKey());
        }

        _adapter = new ImageSelectListAdapter(this, R.layout.image_select_list_row, listOfItems);
        _imageList.setAdapter(_adapter);
        ListViewUtils.setListViewHeightBasedOnChildren(_imageList);
        _imageList.setOnItemClickListener(_adapter);
    }

    protected void onResume() {
        super.onResume();
        _question.setText(_puzzle.getQuestion());
        TimerUtil.startTimer(_puzzle.getPuzzleTime(), _counter, this, MendeluMurderStoryLineDBHelper.class);

        Task task = _storyLine.currentTask();
        if (task == null ){
            _taskName.setText(R.string.no_task);
        } else {
            _taskName.setText(task.getName());
        }
    }
}