package cz.mendelu.busItWeek;

import cz.mendelu.busItWeek.library.StoryLineDatabaseHelper;
import cz.mendelu.busItWeek.library.builder.StoryLineBuilder;

/**
 * Created by 11500036 on 3/04/2017.
 */

public class MyDemoStoryLineDbHelper extends StoryLineDatabaseHelper {

    public MyDemoStoryLineDbHelper() {
        super(38);
    }

    @Override
    protected void onCreate(StoryLineBuilder builder) {
        builder.addGPSTask("Mendelu Murder Chapter VI: test of the tests")
                .victoryPoints(1)
                .radius(500000)
                .location(49.209519, 16.61441)
                .hint("hint")
                .choicePuzzle()
                    .puzzleTime(90000)
                    .question("Dit is eeen vraag, kies het antwoord")
                    .addChoice("Antwoord 1", false)
                    .addChoice("antwoord 2", false)
                    .addChoice("antwoord 3", true)
                    .puzzleDone()
                .taskDone();

        builder.addGPSTask("Mendelu Murder Chapter I: test of the tests")
                .victoryPoints(10)
                .location(49.209519, 16.61441)
                .hint("hint")
                .radius(500000)
                .simplePuzzle()
                    .question("1 + 3 = ?")
                    .answer("4")
                    .puzzleDone()
                .taskDone();

        builder.addGPSTask("Mendelu Murder Chapter II: test of the tests")
                .location(49.209519, 16.614410)
                .radius(300000)
                .victoryPoints(5)
                .imageSelectPuzzle()
                    .puzzleTime(500000)
                    .question("Choose an image")
                    .addImage(R.drawable.sus1, false)
                    .addImage(R.drawable.sus2, true)
                    .addImage(R.drawable.sus3, false)
                    .addImage(R.drawable.sus4, false)
                    .addImage(R.drawable.sus5, false)
                    .addImage(R.drawable.sus6, false)
                    .puzzleDone()
                .taskDone();

        builder.addBeaconTask("Mendelu Murder Chapter III: test of the tests")
                .beacon(1,9)
                .victoryPoints(15)
                .location(49.209519, 16.614410)
                .simplePuzzle()
                    .question("What is the meaning of life")
                    .answer("BusIt Week")
                    .puzzleTime(500000)
                    .puzzleDone()
                .taskDone();

        builder.addCodeTask("Mendelu Murder Chapter IV: test of the tests")
                .location(49.209519, 16.614410)
                .victoryPoints(13)
                .qr("Mendel")
                .simplePuzzle()
                    .question("What is the best town in the world")
                    .answer("Genk")
                    .puzzleTime(15000)
                    .puzzleDone()
                .taskDone();
    }
}
