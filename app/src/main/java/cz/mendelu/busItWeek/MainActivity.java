package cz.mendelu.busItWeek;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import cz.mendelu.busItWeek.library.StoryLine;
import cz.mendelu.busItWeek.library.Task;

public class MainActivity extends AppCompatActivity {

    // Variables
    private static final int REQUEST_PERMISSION = 100;
    private TextView _taskName;
    private StoryLine _storyLine;

    // Public
    public void showPuzzle(View view){
        Intent intent = new Intent(this, MapActivity.class);
        startActivity(intent);
    }

    public void showClues(View view){
        Intent intent = new Intent(this, ClueActivity.class);
        startActivity(intent);
    }

    // Protected
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _taskName = (TextView) findViewById(R.id.taskName);
        _storyLine = _storyLine.open(this, MendeluMurderStoryLineDBHelper.class);

        boolean hasPermission = (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)) == PackageManager.PERMISSION_GRANTED;
        if (!hasPermission){
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION );
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        Task task = _storyLine.currentTask();

        if (task == null ){
            //_taskName.setText(R.string.no_task);
        } else {
           // _taskName.setText(task.getName());
        }
    }
}
