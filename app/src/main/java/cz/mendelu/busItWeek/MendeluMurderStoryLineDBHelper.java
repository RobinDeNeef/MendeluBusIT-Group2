package cz.mendelu.busItWeek;

import cz.mendelu.busItWeek.library.StoryLineDatabaseHelper;
import cz.mendelu.busItWeek.library.builder.StoryLineBuilder;

/**
 * Created by 11500036 on 6/04/2017.
 */

public class MendeluMurderStoryLineDBHelper extends StoryLineDatabaseHelper {

    // Variables
    private static final int _RADIUS = 30;

    // Constructor
    public MendeluMurderStoryLineDBHelper() {
        super(14);
    }

    // Protected
    @Override
    protected void onCreate(StoryLineBuilder builder) {
        builder.addCodeTask("Mendelu Murder Chapter I: Beginning")
                .location(49.210233, 16.614575)
                .victoryPoints(10)
                .qr("Mendelu Murder 1")
                .simplePuzzle()
                    .question("The more you take, the more you leave behind. What am I?")
                    .answer("Footsteps")
                    .puzzleTime(90000)
                    .puzzleDone()
                .taskDone();

        builder.addGPSTask("Mendelu Murder Chapter II: Bury Your Dead")
                .location(49.210923, 16.616434)
                .victoryPoints(10)
                .radius(_RADIUS)
                .simplePuzzle()
                    .question("I'm seen by the one who created me, I'm seen by the one who creates me, I'm seen by the one who purchases me. But, I'm not seen by the one who uses me. What am I?")
                    .answer("Coffin")
                    .puzzleTime(90000)
                    .puzzleDone()
                .taskDone();

        builder.addBeaconTask("Mendelu Murder Chapter III: Hush Hush")
                .location(49.209815, 16.616280)
                .victoryPoints(10)
                .beacon(1, 2)
                .choicePuzzle()
                    .question("Mr Brown was killed on Sunday afternoon. Who is the killer?")
                    .addChoice("The wife said she was reading a book.", false)
                    .addChoice("The butler said He was taking a shower.", false)
                    .addChoice("The chef said he was making breakfast.", true)
                    .addChoice("The maid said she was folding clothes", false)
                    .addChoice("the gardener said he was planting tomatoes.", false)
                    .puzzleTime(90000)
                    .puzzleDone()
                .taskDone();

        builder.addGPSTask("Mendelu Murder Chapter IV: Something Broken")
                .location(49.209892, 16.613554)
                .victoryPoints(10)
                .radius(_RADIUS)
                .simplePuzzle()
                    .question("What begins with T, ends with T and has T in it?")
                    .answer("Teapot")
                    .puzzleTime(90000)
                    .puzzleDone()
                .taskDone();

        builder.addGPSTask("Mendelu Murder Chapter V: Live By Night")
                .location(49.210222, 16.614583)
                .victoryPoints(10)
                .radius(_RADIUS)
                .simplePuzzle()
                    .question("I see without seeing, to me, darkness is as clear as daylight. What am I?")
                    .answer("Bat")
                    .puzzleTime(90000)
                    .puzzleDone()
                .taskDone();

        builder.addCodeTask("Mendelu Murder Chapter VI: Revelations")
                .location(49.211226, 16.614536)
                .victoryPoints(25)
                .qr("Mendelu Murder 2")
                .imageSelectPuzzle()
                    .puzzleTime(500000)
                    .question("Who is the Mendelu Ripper?")
                    .addImage(R.drawable.sus1, false)
                    .addImage(R.drawable.sus2, true)
                    .addImage(R.drawable.sus3, false)
                    .addImage(R.drawable.sus4, false)
                    .addImage(R.drawable.sus5, false)
                    .addImage(R.drawable.sus6, false)
                    .puzzleDone()
                .taskDone();
    }
}
