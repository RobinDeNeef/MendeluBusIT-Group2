package cz.mendelu.busItWeek;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageButton;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.SphericalUtil;

import java.util.HashMap;
import java.util.Map;

import cz.mendelu.busItWeek.library.BeaconTask;
import cz.mendelu.busItWeek.library.ChoicePuzzle;
import cz.mendelu.busItWeek.library.CodeTask;
import cz.mendelu.busItWeek.library.GPSTask;
import cz.mendelu.busItWeek.library.ImageSelectPuzzle;
import cz.mendelu.busItWeek.library.Puzzle;
import cz.mendelu.busItWeek.library.SimplePuzzle;
import cz.mendelu.busItWeek.library.StoryLine;
import cz.mendelu.busItWeek.library.Task;
import cz.mendelu.busItWeek.library.beacons.BeaconDefinition;
import cz.mendelu.busItWeek.library.beacons.BeaconUtil;
import cz.mendelu.busItWeek.library.map.MapUtil;
import cz.mendelu.busItWeek.library.qrcode.QRCodeUtil;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, GoogleMap.OnMarkerClickListener {

    // Variables
    private static final int SIMPLE_PUZZLE_REQUEST_CODE = 1;
    private static final int IMAGE_PUZZLE_REQUEST_CODE = 2;
    private static final int CHOICE_PUZZLE_REQUEST_CODE = 3;
    private GoogleMap _mMap;
    private StoryLine _storyLine;
    private Task _currentTask;
    private GoogleApiClient _googleApiClient;
    private LocationRequest _locationrequest;
    private HashMap<Task, Marker> _mapOfMarkers;
    private BeaconUtil _beaconUtil;
    private ImageButton readQRCodeButton;

    // Public
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        _mMap = googleMap;

        initialiseTasks();

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            // return;
        }

        _mMap.setMyLocationEnabled(true);
        _mMap.setOnMarkerClickListener(this);
        _mMap.getUiSettings().setMyLocationButtonEnabled(true);
        _mMap.getUiSettings().setMapToolbarEnabled(false);
        _mMap.setMinZoomPreference(6.0f);
        //_mMap.setMaxZoomPreference(14.0f);
        _mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(49.209519, 16.614410), 14.0f));
    }

    @Override
    public void onLocationChanged(Location location) {
        if (_currentTask != null && _currentTask instanceof GPSTask) {
            double radius = ((GPSTask) _currentTask).getRadius();
            LatLng user = new LatLng(location.getLatitude(), location.getLongitude());
            LatLng taskPosition = new LatLng(_currentTask.getLatitude(), _currentTask.getLongitude());

            if (SphericalUtil.computeDistanceBetween(user, taskPosition) < radius) {
                runPuzzleActivity(_currentTask.getPuzzle());
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        AlertDialog.Builder alertdialogbuilder = new AlertDialog.Builder(this);
        alertdialogbuilder
                .setTitle("Skip task")
                .setMessage("Do you want to skip the current task?")
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        _currentTask.skip();

                        _currentTask = _storyLine.currentTask();
                        if (_currentTask == null) {
                            // Game is finished
                            // Start final question
                        } else {
                            cancelListeners();
                            initialiseListeners();
                        }

                        updateMarkers();

                        if (_googleApiClient.isConnected()) {
                            if (ActivityCompat.checkSelfPermission(MapActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }

                            Location location = LocationServices.FusedLocationApi.getLastLocation(_googleApiClient);
                            if (_currentTask != null){
                                zoomToNewTask(new LatLng(location.getLatitude(), location.getLongitude()), new LatLng(_currentTask.getLatitude(), _currentTask.getLongitude()));
                            }

                        }
                    }
                });

        AlertDialog dialog = alertdialogbuilder.create();
        dialog.show();

        return false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        initialiseListeners();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void readQRCode(View view){
        QRCodeUtil.startQRScan(this);
    }

    // Protected
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        _storyLine = StoryLine.open(this, MendeluMurderStoryLineDBHelper.class);

        _googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        _googleApiClient.connect();

        _locationrequest = LocationRequest.create();
        _locationrequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        _locationrequest.setInterval(5000);

        _mapOfMarkers = new HashMap<>();
        _beaconUtil = new BeaconUtil(this);
        readQRCodeButton = (ImageButton) findViewById(R.id.readQRButton);
    }

    @Override
    protected void onResume() {
        super.onResume();

        _currentTask = _storyLine.currentTask();
        if (_currentTask == null) {
            // Game is finished, no more tasks
            // Final question
        } else {
            // More tasks
            initialiseListeners();
        }

        updateMarkers();
    }

    @Override
    protected void onPause() {
        super.onPause();
        cancelListeners();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (_currentTask != null && _currentTask instanceof CodeTask){
            String result = QRCodeUtil.onScanResult(this, requestCode, resultCode, data);
            CodeTask codeTask = (CodeTask) _currentTask;

            if (codeTask.getQR().equals(result)){
                runPuzzleActivity(_currentTask.getPuzzle());
            }
        }
    }

    // Private
    private void initialiseTasks() {
        for (Task task : _storyLine.taskList()) {
            Marker newMarker = null;

            if (task instanceof GPSTask) {
                newMarker = MapUtil.createColoredCircleMarker(
                        this,
                        _mMap,
                        task.getName(),
                        R.color.colorGPS,
                        R.style.marker_text_style,
                        new LatLng(task.getLatitude(), task.getLongitude())
                );
            }

            if (task instanceof BeaconTask){
                newMarker = MapUtil.createColoredCircleMarker(
                        this,
                        _mMap,
                        task.getName(),
                        R.color.colorBeacon,
                        R.style.marker_text_style,
                        new LatLng(task.getLatitude(), task.getLongitude())
                );

                BeaconDefinition definition = new BeaconDefinition((BeaconTask) task) {
                    @Override
                    public void execute() {
                        runPuzzleActivity(_currentTask.getPuzzle());
                    }
                };
                _beaconUtil.addBeacon(definition);
            }

            if (task instanceof CodeTask){
                newMarker = MapUtil.createColoredCircleMarker(
                        this,
                        _mMap,
                        task.getName(),
                        R.color.colorQR,
                        R.style.marker_text_style,
                        new LatLng(task.getLatitude(), task.getLongitude())
                );
            }

            //newMarker.setVisible(false);
            _mapOfMarkers.put(task, newMarker);
        }
        updateMarkers();
    }

    private void initialiseListeners() {
        if (_currentTask != null) {
            if (_currentTask instanceof GPSTask) {
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                if (_googleApiClient.isConnected()){
                    LocationServices.FusedLocationApi
                            .requestLocationUpdates(
                                    _googleApiClient,
                                    _locationrequest,
                                    this);
                }
            }

            if (_currentTask instanceof BeaconTask){
                _beaconUtil.startRanging();
            }

            if (_currentTask instanceof CodeTask){
                readQRCodeButton.setVisibility(View.VISIBLE);
            } else {
                readQRCodeButton.setVisibility(View.GONE);
            }
        }
    }

    private void updateMarkers() {

        for (Map.Entry<Task, Marker> entry : _mapOfMarkers.entrySet()){
            if (_currentTask != null){
                if (entry.getKey().getName().equals(_currentTask.getName())){
                    entry.getValue().setVisible(true);
                } else {
                    entry.getValue().setVisible(false);
                }
            } else {
                entry.getValue().setVisible(false);
            }
        }
    }

    private void cancelListeners() {
        LocationServices.FusedLocationApi.removeLocationUpdates(_googleApiClient, this);

        if(_beaconUtil.isRanging()){
            _beaconUtil.stopRanging();
        }

        if (_currentTask instanceof CodeTask){
            readQRCodeButton.setVisibility(View.GONE);
        }
    }

    private void runPuzzleActivity(Puzzle puzzle) {
        if (puzzle instanceof SimplePuzzle) {
            Intent intent = new Intent(this, SimplePuzzleActivity.class);
            startActivityForResult(intent, SIMPLE_PUZZLE_REQUEST_CODE);
        }

        if (puzzle instanceof ImageSelectPuzzle) {
            Intent intent = new Intent(this, ImageSelectPuzzleActivity.class);
            startActivityForResult(intent, IMAGE_PUZZLE_REQUEST_CODE);
        }

        if (puzzle instanceof  ChoicePuzzle){
            Intent intent = new Intent(this, ChoicePuzzleActivity.class);
            startActivityForResult(intent, CHOICE_PUZZLE_REQUEST_CODE);
        }
    }

    private void zoomToNewTask(LatLng userPosition, LatLng taskPosition){
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(userPosition);
        builder.include(taskPosition);
        LatLngBounds bounds = builder.build();

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 100);
        _mMap.animateCamera(cameraUpdate);
    }
}
