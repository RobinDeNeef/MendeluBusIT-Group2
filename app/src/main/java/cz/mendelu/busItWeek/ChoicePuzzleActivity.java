package cz.mendelu.busItWeek;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cz.mendelu.busItWeek.library.ChoicePuzzle;
import cz.mendelu.busItWeek.library.StoryLine;
import cz.mendelu.busItWeek.library.Task;
import cz.mendelu.busItWeek.library.listview.ListViewUtils;
import cz.mendelu.busItWeek.library.timer.TimerUtil;


public class ChoicePuzzleActivity extends AppCompatActivity {
    private TextView _taskName;
    private TextView _counter;
    private TextView _question;
    private ListView _listView;
    private StoryLine _storyline;
    private ChoicePuzzle _puzzle;
    private Task _currentTask;
    private ChoiceSelectAdapter _adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice_puzzle);

        _question = (TextView) findViewById(R.id.question);
        _counter = (TextView) findViewById(R.id.counter);
        _storyline = StoryLine.open(this, MendeluMurderStoryLineDBHelper.class);
        _currentTask = _storyline.currentTask();
        _puzzle = (ChoicePuzzle) _currentTask.getPuzzle();
        _listView = (ListView) findViewById(R.id.choicelist);
        _taskName = (TextView) findViewById(R.id.taskName);

        List<String> listOfItems = new ArrayList<>();
        for (Map.Entry<String,Boolean> entry : _puzzle.getChoices().entrySet()){
            listOfItems.add(entry.getKey());
        }

        _adapter = new ChoiceSelectAdapter(this, R.layout.choice_list_row,listOfItems);
        _listView.setAdapter(_adapter);
        ListViewUtils.setListViewHeightBasedOnChildren(_listView);
    }


    public class ChoiceSelectAdapter extends ArrayAdapter<String>{

        public ChoiceSelectAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<String> objects) {
            super(context, resource, objects);
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if(convertView == null){
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.choice_list_row, null);
            }

            final String choice = getItem(position);
            if (choice != null){
                Button button = (Button) convertView.findViewById(R.id.button);
                button.setText(choice);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (_puzzle.getAnswerForChoice(position)){
                            LayoutInflater inflater = getLayoutInflater();
                            View layout = inflater.inflate(R.layout.toast, (ViewGroup) findViewById(R.id.toast_layout_root));

                            TextView text = (TextView) layout.findViewById(R.id.text);
                            text.setText("Clue found!");

                            Toast toast = new Toast(getApplicationContext());
                            toast.setGravity(Gravity.BOTTOM, 10, 50);
                            toast.setDuration(Toast.LENGTH_LONG);
                            toast.setView(layout);
                            toast.show();

                            _currentTask.finish(true);
                            TimerUtil.stopTimer();
                            finish();

                        }else{
                            _currentTask.finish(false);
                            TimerUtil.stopTimer();
                            finish();
                        }
                    }
                });
            }

            return convertView;
        }
    }

    protected void onResume() {
        super.onResume();
        TimerUtil.startTimer(_puzzle.getPuzzleTime(), _counter, this, MendeluMurderStoryLineDBHelper.class);
        _question.setText(_puzzle.getQuestion());

        Task task = _storyline.currentTask();

        if (task == null ){
            _taskName.setText(R.string.no_task);
        } else {
            _taskName.setText(task.getName());
        }

    }
}